import { Component, OnInit } from '@angular/core';
import {TvShowsService} from '../../services/TvShows.service';

@Component({
  selector: 'app-detail-info',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  constructor(private service: TvShowsService) { }

  ngOnInit() {
  }

  get show() {
    return this.service.detailShow;
  }

}
